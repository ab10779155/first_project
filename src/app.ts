import CustomButton from './components/controls/buttons/custom-button/custom-button.component'
import {CustomInput, spanWithError} from './components/controls/custom-input/custom-input.component'
import {elementsBlock} from './components/elements/elements.component'

import Utils from './utils/utils'

function App(): void  { 

    let loading: boolean = false; 
    let inputState: string; // значание (value) инпута 
    let inputEmailElement: HTMLInputElement;
    let inputStatusFocus: Event; 
    let componentContainer: HTMLDivElement | null = render(); 


    function onClick(): void { 
        loading = !loading; 
        componentContainer = render(); 
         
        const inputEmailValueWithoutSpace:string = inputEmailElement.value.trim();
        
        const element: HTMLElement = Utils.createCustomHtmlElement<HTMLElement, string>({ tag: 'li', className: 'element', id: 'element', content: inputEmailValueWithoutSpace});

        if (inputEmailValueWithoutSpace) {
            inputState = '';
            elementsBlock.appendChild(element);
        } 
    }


    function inputChange(e: Event): void { 
        
        inputState = (e.target as HTMLInputElement).value; //строковая переменная отлавливает изменения в инпуте и сохраняет значение (value)

        inputStatusFocus = e; // сохраняем в перременную состояние  фокуса - InputEvent - (при удалении последнего символа из инпута фокус не теряется)
        
        componentContainer = render(); //ререндер после каждого нажатия клавиши
    }

    function render(): HTMLDivElement | null  { 
        const appContainer: HTMLDivElement | null = document.querySelector('#App'); 
        if (appContainer) {
            if (appContainer.innerHTML) {  
                appContainer.innerHTML = ''; 
        }

    
        appContainer.append(CustomButton({ 
            label: 'Отправить', 
            typeStyle: 'primary', 
            icon: 'X',
            iconPosition: 'left',
            onClick,
            className: 'widthBtn',
            loading
        }));

        
        appContainer.append(elementsBlock);

        const inputWrapper = Utils.createCustomHtmlElement<HTMLInputElement, string>({ tag: 'div', className: 'input-wrapper', id: 'wrapper'});

        inputWrapper.append(CustomInput({
            errorMessage: true, 
            requaredValidator: 'requared', 
            typeInput: 'email',
            maxLength: 200, 
            placeholder: 'введите вашу почту', 
            onChange: inputChange, 
            value: inputState 
        }));

        inputWrapper.append(spanWithError);
        appContainer.prepend(inputWrapper);


        inputEmailElement = document.querySelector('#email') as HTMLInputElement; // находим инпут 

      
        if (inputStatusFocus) { // это InputEvent - это интерфейс представляющий события, происходящие когда пользователь вводит данные
            inputEmailElement.focus(); // эта строчка при вводе символов и ререндере оставляет фокус на инпуте
        }

    }

        return appContainer; 
    }
}

export default App;