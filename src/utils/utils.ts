import { TCustomHtmlElement } from './types';

class Utils { 

    static createCustomHtmlElement<T extends HTMLElement, C  extends HTMLElement | string>({tag, className, id, content }: TCustomHtmlElement<C> ): T {  

        let newHtmlElement: T; 
        if (tag) { 
            newHtmlElement = document.createElement(tag) as T; 
        } else {
            throw Error('A tag property is require') 
        }
        
        if (className) { 
            newHtmlElement.className = className; 
        }
        if (id) { 
            newHtmlElement.id = id; 
        }
        if (content) {
            newHtmlElement.prepend(content); 
        }

        return newHtmlElement;
    }
}

export default Utils;