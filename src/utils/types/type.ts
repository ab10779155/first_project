
export type TCustomHtmlElement<T extends HTMLElement | string> = {
    tag: string, 
    className: string, 
    id: string,
    content?:  T,
    value?: string
} 