import Utils from '../../../../utils/utils';
import { TCustomButton } from './types';




function CustomButton({ label, typeStyle, icon, iconPosition, onClick, className, loading }: TCustomButton): HTMLButtonElement  {
    const buttonElement: HTMLButtonElement = Utils.createCustomHtmlElement<HTMLButtonElement, string>({ 
        tag: 'button', 
        className: 'custom-button-component', 
        id: 'button', 
        content: label
    });
    
    if (typeStyle) {
        buttonElement.classList.add(typeStyle);
    }

    if (icon && iconPosition === 'right') {
        buttonElement.append(Utils.createCustomHtmlElement<HTMLDivElement, string>({ tag: 'div', className: 'button-icon', id: 'icon',content: icon}));
    } else if (iconPosition === 'left') {
        buttonElement.prepend(Utils.createCustomHtmlElement<HTMLDivElement, string>({ tag: 'div', className: 'button-icon', id: 'icon',content: icon}));   
    }  else {
        throw Error('The IconPosition is require property \'left\' or \'right\'') 
    }
    

    if (className) {
        buttonElement.classList.add(className);
    }

    if (loading) {
        const spinnerElement: HTMLDivElement = Utils.createCustomHtmlElement<HTMLDivElement, string >({ tag: 'div', className: 'spinner', id: 'spinner' });
        buttonElement.innerHTML = '';
        buttonElement.append(spinnerElement);
    }

    buttonElement.addEventListener('click', onClick);
  

    return buttonElement; 
}


export default CustomButton;