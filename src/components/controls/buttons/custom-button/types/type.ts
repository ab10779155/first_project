
export type TCustomButton = {
    label: string, 
    typeStyle: string, 
    icon: string, 
    iconPosition: string,
    onClick: ()=> void, 
    className: string, 
    loading: boolean 
} 