import Utils from '../../../utils/utils';
import { TCustomInput } from './types';


export const spanWithError = Utils.createCustomHtmlElement<HTMLElement, string>({ tag: 'span', className: 'error', id: 'span', content: ''});
export function CustomInput({errorMessage, requaredValidator, typeInput, maxLength, placeholder, onChange, value }: TCustomInput) : HTMLInputElement {
    let inputElement: HTMLInputElement = Utils.createCustomHtmlElement<HTMLInputElement, string>({ 
                tag: 'input', 
                className: 'custom-input-component', 
                id: 'email',
            });
            console.log(inputElement.validity);
            
      
    if (errorMessage) {

        if (requaredValidator) {
            inputElement.setAttribute(requaredValidator, '');  
        }       
        
        if (typeInput) {
            inputElement.type = typeInput;
        }
    
        if (maxLength) {
            inputElement.maxLength = maxLength
        }
    } else if (!errorMessage) {
        inputElement.removeAttribute('type');
        inputElement.removeAttribute('requared');
        spanWithError.classList.remove('error_visible');
    } 
    
    if (placeholder) {
        inputElement.placeholder = placeholder; 
    }

    if (value) {     
        inputElement.value = value;
        
    } 

    inputElement.addEventListener('input', onChange);

    inputElement.addEventListener('focus', (event: Event) => {
       
        
        if (!(event.target as HTMLInputElement).value) {
            spanWithError.textContent = 'Пожалуйста заполните это поле';
        }
        else if (inputElement.validity.typeMismatch) {
            spanWithError.textContent = 'невалидный e-mail';
            spanWithError.classList.add('error_visible');
        } 
        else if (!inputElement.validity.typeMismatch) {
            spanWithError.classList.remove('error_visible');  
            spanWithError.textContent = ''; 
        } 
        else if (inputElement.validity.tooLong) {
            spanWithError.textContent = `Поле email должно быть не более ${inputElement.maxLength} символов.`;
        } 
      });


    inputElement.addEventListener('blur', (event: Event) => {
        
        if (!inputElement.validity.valid) {
            spanWithError.textContent = 'Пожалуйста заполните это поле';
            spanWithError.classList.add('error_visible');
        }
    });

    return inputElement; 
}

