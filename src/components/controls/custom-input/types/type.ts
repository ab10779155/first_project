export type TCustomInput = {
    errorMessage?: boolean, 
    requaredValidator?: string,
    typeInput: string, 
    maxLength: number, 
    placeholder: string,
    onChange: (e: Event) => void,
    value?: string,
} 