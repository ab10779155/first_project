import Utils from '../../utils/utils';

export const elementsBlock: HTMLDivElement = Utils.createCustomHtmlElement<HTMLDivElement, string>({ tag: 'ul', className: 'elements', id: 'elements'});

